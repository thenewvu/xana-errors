'use strict';

const inspect = require('xana-inspect');

/**
 * Base class for handled error classes.
 */
class HandledError {
  constructor(message, detail) {
    this.type    = this.constructor.name;
    this.message = message;
    this.detail  = detail instanceof Error ? inspect(detail) : detail;
  }
}

/**
 * ValidationError.
 */
class ValidationError extends HandledError {
  constructor(detail) {
    super(undefined, detail);
  }
}

/**
 * InternalServerError.
 */
class InternalServerError extends HandledError {
  constructor(message, detail) {
    super(message, detail);
  }
}

/**
 * OperationalError.
 */
class OperationalError extends HandledError {
  constructor(message, detail) {
    super(message, detail);
  }
}

module.exports = {
  HandledError,
  ValidationError,
  InternalServerError,
  OperationalError
};
